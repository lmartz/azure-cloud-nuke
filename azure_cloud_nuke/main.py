from pathlib import Path

import typer
import yaml
from azure.core.exceptions import ResourceNotFoundError
from azure.identity import DefaultAzureCredential
from azure.mgmt.resource import ResourceManagementClient
from schema import Schema
from schema import SchemaError

app = typer.Typer(help="Azure CLI resources / resources groups Nuker.")


def validate_conf_file(file: Path) -> dict:
    config_schema = Schema(
        {"protected": {"resource_name": list, "resource_type": list}}
    )

    with open(file) as f:
        conf_yaml = yaml.safe_load(f)
    try:
        config_schema.validate(conf_yaml)

    except SchemaError as se:
        typer.echo(f"Invalid YAML: {se}", err=True)
        raise se

    return conf_yaml


def get_latest_api_version(provider_namespace, client) -> str:
    rsc_provider, rsc_space = (
        provider_namespace.split("/")[0],
        provider_namespace.split("/")[-1],
    )
    provider = client.providers.get(resource_provider_namespace=rsc_provider)
    return [
        max(i.api_versions)
        for i in provider.resource_types
        if i.resource_type == rsc_space
    ][0]


def initiate_connection(subscription_id: str) -> ResourceManagementClient:
    credentials = DefaultAzureCredential()
    return ResourceManagementClient(credentials, subscription_id)


def nuke(
    kind: str,
    subscription_id: str,
    unattended: bool,
    protect: list,
    protect_conf: dict,
    resource_group: str = None,
    pytest_mode: bool = False,
):
    client = initiate_connection(subscription_id)
    iterable = (
        client.resources.list_by_resource_group(resource_group)
        if kind == "rg"
        else client.resource_groups.list()
    )
    nuke_count = 0
    if not unattended:
        typer.confirm(
            "Are you sure you want to proceed with deletion of resources",
            abort=True,
        )
    try:
        with typer.progressbar(list(iterable), label="Nuking") as progress:
            for resource in progress:
                if resource.name not in protect_conf.get(
                    "protected", None
                ).get(
                    "resource_name", None
                ) and resource.type not in protect_conf.get(
                    "protected", None
                ).get(
                    "resource_type", None
                ):
                    provider_api_version = (
                        get_latest_api_version(resource.type, client)
                        if kind == "rg"
                        else None
                    )

                    typer.echo(
                        f"\tCurrently nuking {resource.name} in ResourceGroup: \
{resource_group}"
                    ) if kind == "rg" else typer.echo(
                        f"\tCurrently nuking {resource.name} \
ResourceGroup"
                    )
                    if not pytest_mode:
                        delete_async_operation = (
                            client.resources.begin_delete_by_id(
                                resource_id=resource.id,
                                api_version=provider_api_version,
                            )
                            if kind == "rg"
                            else client.resource_groups.begin_delete(
                                resource.name
                            )
                        )
                        delete_async_operation.wait()
                    nuke_count += 1
                    typer.echo(f"Processed {nuke_count} resources.")
                else:
                    if (
                        resource.type
                        in protect_conf["protected"]["resource_type"]
                    ):
                        typer.secho(
                            f"\t{resource.name} is of type {resource.type} which\
is in the protected resources types list, \
SKIPPING.\n",
                            fg=typer.colors.GREEN,
                        )

                    else:
                        typer.secho(
                            f"\t{resource.name} is in the protected resources names list, \
SKIPPING.\n",
                            fg=typer.colors.GREEN,
                        )
    except ResourceNotFoundError as e:
        print(f"{e}")
    if nuke_count > 0:
        typer.echo(
            f"{nuke_count} resourceGroup(s) have been nuked."
        ) if kind == "sub" else typer.echo(
            f"{nuke_count} resource(s) have been nuked."
        )
    else:
        typer.echo("Nothing to be nuked here.")
        # sys.exit(0)


@app.command("nuke-it-all")
def nuke_all_in_sub(
    subscription_id: str = typer.Argument(
        ...,
        envvar="NUKE_SUBSCRIPTION_ID",
        help="Specifies in which Azure subscription the nuke will take place.",
    ),
    unattended: bool = typer.Option(
        False, help="Force deletion without confirmation."
    ),
    protect: bool = typer.Option(
        False,
        help="Protect resource from being nuked, \
        ",
    ),
    protect_conf_file: Path = typer.Option(
        None,
        help="Path to the config file \
        ",
    ),
) -> None:
    """
    Nuke all the RG existing in the specified Azure subscription.

    If --unattended is used, will nuke them all without confirmation.
    """
    protect_conf = (
        validate_conf_file(protect_conf_file)
        if protect
        else {"protected": {"resource_name": [], "resource_type": []}}
    )
    nuke("sub", subscription_id, unattended, protect, protect_conf)


@app.command("nuke-in-rg")
def nuke_rsc_in_resource_group(
    resource_group: str = typer.Argument(
        ..., help="Specifies in which ResourceGroup the nuke will take place."
    ),
    subscription_id: str = typer.Argument(
        ...,
        envvar="NUKE_SUBSCRIPTION_ID",
        help="Specifies in which Azure subscription the nuke will take place. \
        Can be sourced from environnement variable instead",
    ),
    unattended: bool = typer.Option(
        False, help="Force deletion without confirmation."
    ),
    protect: bool = typer.Option(
        False,
        help="Protect resource from being nuked, \
        ",
    ),
    protect_conf_file: Path = typer.Option(
        None,
        help="Path to the config file \
        ",
    ),
) -> None:
    """
    Nuke all the resources within the specified Azure ResourceGroup.

    If --unattended is used, will nuke them all without confirmation.
    """
    protect_conf = (
        validate_conf_file(protect_conf_file)
        if protect
        else {"protected": {"resource_name": [], "resource_type": []}}
    )
    nuke(
        "rg",
        subscription_id,
        unattended,
        protect,
        protect_conf,
        resource_group,
    )


if __name__ == "__main__":
    app()
