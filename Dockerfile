ARG ubuntu_version=21.04
#Setting up a light base image
FROM ubuntu:${ubuntu_version} as base
ENV PYTHON_VERSION="3.9.4-1"
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    python3-pip="$(apt-cache madison python3-pip | awk '{ print $3 }')" \
    python3=${PYTHON_VERSION} \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

#Build Stage
FROM base as builder
ENV POETRY_VERSION="1.1.13" \
    POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION="1" \
    PIP_NO_CACHE_DIR="off" \
    PIP_DISABLE_PIP_VERSION_CHECK="on"
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    curl="$(apt-cache madison curl | awk '{ print $3 }'| head -n1)"  \
    && curl -Lo /tmp/get-poetry.py https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py \
    && python3 /tmp/get-poetry.py \
    && rm -f /tmp/get-poetry.py
WORKDIR /src
COPY . .
RUN $POETRY_HOME/bin/poetry build

#Installing from build stage, running from base image
FROM base as runtime
ARG VERSION
ENV PIP_NO_CACHE_DIR="off" \
    PIP_DISABLE_PIP_VERSION_CHECK="on"
COPY --from=builder /src/dist /src
WORKDIR /app
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN pip install --no-cache-dir /src/azure_cloud_nuke-${VERSION}-py3-none-any.whl \
        && rm -rf /src/ \
        && groupadd -g 10100 az-nuker \
        && useradd -m -s /bin/bash -g 10100 -u 10100 az-nuker \
        && chown az-nuker: -R /app \
        && chmod 700 -R /app

USER az-nuker
ENTRYPOINT [ "az-cloud-nuke" ]
