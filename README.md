# Azure Cloud Nuke

Azure cloud nuke is the tool that automatically destroy azure resources, you can run it on a schedule to avoid being charged huge amounts for resources that you forgot to delete.
You want to run it on test subscriptions, where you have no production workloads and where you test your infra as code modules.
Tools such as terratest are amazing but they sometimes fail without deleting the resources, and you might end up being billed for them.

The azure cloud nuke is based off the excellent [aws cloud nuke](https://github.com/gruntwork-io/cloud-nuke) and is written in python.

![image info](pictures/nuking.png)

## DISCLAIMER

This tool is highly destructive, you MUST know what you are doing with it as you may wipe an entire subscription in a single command.
Please read the documentation carefully and never use it in a subscription that runs production workloads.

## Usage

Whether you install it with pip or run it with docker, the core usage is the same.
The following part will explain the different commands you can use.
az-cloud-nuke ships with two modes:

- one that target a specific resource groups and will parse all the resources in it to destroy them.
- one that parses all the resources-groups within a subscription to destroy them.

The main difference is that the first mode does not "see" at the resources level, it only see resource-groups and delete them, and the second mode can see the resources as it is scoped at the specified resource-group level.
This detail will have its importance when we'll describe the usage of the yaml config file, a file that allows you to protect some resources from deletion based on their names or kind.

### Authentication

The az cloud nuke need to be authenticated in azure in order to proceed to Ram it.
The recommended way to authenticate with the azure SDK is to use a service principal and give it the need permission, then use env var to set its credentials.
To create a service principal please use the [Azure documentation](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli).
For a quick try here is the basic command to create a sp with azure cli

```bash
az ad sp create-for-rbac --name sp-az-cloud-nuke --role "Contributor" --scope /subscriptions/mySubscriptionID/
```

NOTE: I dont recommend using the contributor role in the long run, its okay for a test on a unused subscription but please take the time to fine-tune the needed roles and set the correct RBAC for you needs.

When the SP is created the cli will output some values that you need to keep, theses values are the credentials of you sp.
To authenticate with azure :
If using via the CLI, set the env vars as such:

```bash
export AZURE_CLIENT_ID="your_client_id"
export AZURE_TENANT_ID="your_tenant_id"
export AZURE_CLIENT_SECRET="your_client_secret"
```

If using docker set the same env but use the -e flag such as:

```bash
docker container run ... \
  -e AZURE_CLIENT_SECRET="my_client_secret" \
  -e AZURE_TENANT_ID="your_tenant_id" \
  -e AZURE_CLIENT_ID="your_client_id"
```

### Schedule

The very idea of this tool is the be managed via a cronjob so it can run on a schedule and will allows you to wipe forgotten or unused resources and save you a few bucks .

### help

whenever you need help or a refresher on how you can use the tool, you can run :

```bash
az-cloud-nuke --help
```

### Commands

The azure cloud nuke comes with two commands :

```bash
az-cloud-nuke nuke-it-all
az-cloud-nuke nuke-in-rg
```

#### nuke-it-all

this command will absolutely wreck your entire subscription, literally, it will parse all the resources-groups within it and delete them one after another.
It will set the scope of the az-cloud-nuke at the subscription level and will never enters a specific resource-group, it will simply delete them.
You should only use this mode in test subscriptions

This mode need some arguments, you can always display them with the help mode scoped at this command level:

```bash
az-cloud-nuke nuke-it-all --help
```

the required argument is :

```bash
subscription_id: The id of the target subscription
```

you can also choose to set it via the ENV VAR:

```bash
export NUKE_SUBSCRIPTION_ID="my_sub_id"
```

the possible options are :

```bash
--unattended / --no-unattended: Wether you want a confirmation or not before deletion
--protect/--no-protect Wether you want to pass a config file to filter resources from deletion
--protect-conf-file : path, the path to the config file to filter resources from deletion
```

an exemple run could be :

```bash
az-cloud-nuke nuke-it-all "0000-0000-0000-000-0000" --unattended --protect --protect-conf-file my-protect-file.yaml
```

⚠️ NOTE: It is very important to note that with this mode, as we have no vision of the resources level, you can't use the filtering (with the yaml file, we'll see how to use it a bit later in this doc) with the resouce kind, as all the resources at the subscription scope are resources-groups, if you filter it with this kind , you won't be able to delete anything.
You can only filter based on the name of the resource-group.

So let's say you have 4 resources-groups in your sub:

- rg1 -> contains 3 keyvaults
- rg2 -> contains 2 vm and 2 keyvaults
- rg3 -> contains 2 vnets
- rg4 -> contains stuff

if you have a rule in your config file that says:

- protect from deletion resources named rg1
- protect from deletion resources that are keyvaults

The az-cloud-nuke would nuke :

- rg2
- rg3
- rg4

#### nuke-in-rg

this command will set the scope of the az-cloud-nuke in the specified resource-group level and will be able to see all of its resources, and delete them.
This mode is safer to use than the nuke-it-all and you should be able to use it in a subscription that hosts other resources-groups.

This mode need some arguments, you can always display them with the help mode scoped at this command level:

```bash
az-cloud-nuke nuke-in-rg --help
```

the required arguments are :

```bash
subscription_id: The id of the target subscription
resource_group: the name of the resource-group to target
```

you can also choose to set it via the ENV VAR:

```bash
export NUKE_SUBSCRIPTION_ID="my_sub_id"
```

the possible options are :

```bash
--unattended / --no-unattended: Wether you want a confirmation or not before deletion
--protect/--no-protect Wether you want to pass a config file to filter resources from deletion
--protect-conf-file : path, the path to the config file to filter resources from deletion
```

an exemple run could be :

```bash
az-cloud-nuke nuke-in-rg "my-resource-group" "0000-0000-0000-000-0000" --unattended --protect --protect-conf-file my-protect-file.yaml
```

⚠️ NOTE: with this mode as you can parse all the resources in the rg, it is easy to protect resources from deletion either based on their names, or their kind.

So let's say you have a resource-group that you are targeting, and it contains:

- a keyvault name kv1
- a storage account named sa1
- a vm name vm1
- a vnet name vnet1

if you have a rule in your config file that says:

- protect from deletion resources named kv1
- protect from deletion resources that are vm

The az-cloud-nuke would nuke :

- the storage account name sa1
- the vnet named vnet1

### Protect configuration Yaml file

As stated before, you can use flags to protect some resources against deletion.

```bash
--protect
--protect-conf-file
```

Once you use those flags, you have to pass a yaml file describing what to protect.
The yaml file must follow this schema:

A root key named "protected", then two lists that can be empty if needed.
The first list "resources_name" describe the resources that need to be protected against deletion based on their name.
The second list "resources_type" describe the resource type and allow you to protect specifics resources based on their type when using the nuke-in-rg mode.
To find the correct resource type, pleaser refer to the [Azure resources provider list](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/azure-services-resource-providers)

```yaml
protected:
  resource_name:
    - my-needed-rg
    - my-kv
  resource_type:
    - "Microsoft.KeyVault/vaults"
    - "Microsoft.Logic"
```

## Installation

You can install / use the azure-cloud-nuke with pip or docker.

### Installation with pip

Installing it with pip will allow you to run it on your localhost with the CLI.

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install azure-cloud-nuke.

```bash
pip install azure-cloud-nuke --extra-index-url https://gitlab.com/api/v4/projects/37415382/packages/pypi/simple
```

### Installation with docker

The project's docker image use "az-cloud-nuke" as the Entrypoint so you can pass the command and args you want as cmd when running the container such as

```bash
docker container run -it \
  -e NUKE_SUBSCRIPTION_ID=$NUKE_SUBSCRIPTION_ID \
  -e AZURE_CLIENT_ID=$AZURE_CLIENT_ID \
  -e AZURE_TENANT_ID=$AZURE_TENANT_ID \
  -e AZURE_CLIENT_SECRET=$AZURE_CLIENT_SECRET \
  -v "$(pwd)"/protect-file.yaml:/app/protect-file.yaml:ro \
  registry.gitlab.com/lmartz/azure-cloud-nuke:0.0.1 \
  nuke-it-all \
  --protect \
  --unattended \
  --protect-conf-file /app/protect-file.yaml
```

NOTE:

- we pass all the needed env var via the -e flag
- We mount out protect file in /app and we tell az-cloud-nuke to use it from within the container (add a volume for persistence if you don't want to have to specify it everytime)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
