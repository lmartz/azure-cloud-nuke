import os
import re
from pathlib import Path

import pytest
from azure.core.exceptions import HttpResponseError
from azure.identity import DefaultAzureCredential
from azure.mgmt.resource import ResourceManagementClient
from schema import SchemaError
from schema import SchemaMissingKeyError

from azure_cloud_nuke.main import get_latest_api_version
from azure_cloud_nuke.main import nuke
from azure_cloud_nuke.main import validate_conf_file


@pytest.fixture
def valid_yaml_file():
    return (
        Path(__file__).resolve().parent / "mockups_files" / "protect_file.yml"
    )


@pytest.fixture
def client_conn():
    subscription_id = os.environ.get("NUKE_SUBSCRIPTION_ID")
    credentials = DefaultAzureCredential()
    return ResourceManagementClient(credentials, subscription_id)


def test_validate_conf_file(capsys, valid_yaml_file):
    invalid_conf_file_key = (
        Path(__file__).resolve().parent
        / "mockups_files"
        / "invalid_protect_file.yml"
    )
    invalid_conf_file_type = (
        Path(__file__).resolve().parent
        / "mockups_files"
        / "invalid_protect_file2.yml"
    )

    # Returns SchemaMissingKeyError if invalid or missing  protect key
    with pytest.raises(SchemaError) as e:
        validate_conf_file(invalid_conf_file_key)
    assert e.type == SchemaMissingKeyError
    captured = capsys.readouterr()
    # Check if the string "Invalid yaml" is in the output on invalid file
    assert "Invalid YAML:" in captured.err

    # Returns SchemaError if invalid schema within the list
    with pytest.raises(SchemaError) as e:
        validate_conf_file(invalid_conf_file_type)
    assert e.type == SchemaError

    # Returns a dict of loaded yaml if valid schema
    assert isinstance(validate_conf_file(valid_yaml_file), dict)


def test_initiate_connection(client_conn):
    assert isinstance(client_conn, ResourceManagementClient)


def test_get_latest_api_version(client_conn):
    api_version = get_latest_api_version(
        "Microsoft.KeyVault/vaults", client_conn
    )
    print(api_version)
    assert isinstance(api_version, str)
    # check that the outputed string if matchin this format :
    # 2022-02-01-preview
    assert re.search("^[0-9]{4,}-[0-9]{2,}-[0-9]{2,}-.+$", api_version)


def test_nuke(capsys, valid_yaml_file, client_conn):
    subscription_id = os.environ.get("NUKE_SUBSCRIPTION_ID")
    unattended = True
    protect = True
    protect_conf = valid_yaml_file

    # Checking that when kind is rg but resource_group is empty,
    # a valueerror is raised
    with pytest.raises(ValueError) as e:
        nuke(
            "rg",
            subscription_id,
            unattended,
            protect,
            protect_conf,
            resource_group=None,
            pytest_mode=True,
        )
    assert e.type == ValueError

    # Checking that a non existing RG match a specific string
    nuke(
        "rg",
        subscription_id,
        unattended,
        protect,
        protect_conf,
        resource_group="Pytest-rg",
        pytest_mode=True,
    )
    captured = capsys.readouterr()
    assert (
        "(ResourceGroupNotFound) Resource group 'Pytest-rg' \
could not be found."
        in captured.out
    )
    # provisionning a temp RG
    for rg in ["rg-protected", "rg-unprotected"]:
        client_conn.resource_groups.create_or_update(
            f"{rg}", {"location": "canadacentral"}
        )
    # Checking that an exising RG with no resources whithin it
    # will produce a nothing to nuke string
    nuke(
        "rg",
        subscription_id,
        unattended,
        protect,
        protect_conf,
        resource_group="rg-protected",
        pytest_mode=True,
    )
    captured = capsys.readouterr()
    assert (
        "Nuking" in captured.out
        and "Nothing to be nuked here." in captured.out
    )

    # Sub
    # Checking that a protected by name resource is in fact protected
    protect_conf = {
        "protected": {"resource_name": ["rg-protected"], "resource_type": []}
    }
    nuke(
        "sub",
        subscription_id,
        unattended,
        protect,
        protect_conf,
        resource_group="None",
        pytest_mode=True,
    )
    captured = capsys.readouterr()
    assert (
        "\trg-protected is in the protected resources names list, SKIPPING.\n"
        in captured.out
    )

    # Checking that an unprotected by name resource is in fact unprotected
    protect_conf = {
        "protected": {"resource_name": ["rg-protected"], "resource_type": []}
    }
    nuke(
        "sub",
        subscription_id,
        unattended,
        protect,
        protect_conf,
        resource_group="None",
        pytest_mode=True,
    )
    captured = capsys.readouterr()
    assert "\tCurrently nuking rg-unprotected ResourceGroup\n" in captured.out

    # Cleaning up
    for i in ["rg-protected", "rg-unprotected"]:
        delete_async_operation = client_conn.resource_groups.begin_delete(i)
        delete_async_operation.wait()
    # Checking that an invalid sub id raise and
    # HttpResponseError and matches a string
    with pytest.raises(HttpResponseError) as e:
        nuke(
            "rg",
            "invalid-sub",
            unattended,
            protect,
            protect_conf,
            resource_group="None",
            pytest_mode=True,
        )
    assert e.type == HttpResponseError
